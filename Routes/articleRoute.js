const express = require("express")
const { getArticlebyId, addArticle, updateArticle, deleteArticle, getAllArticles } = require("../Controllers/articleController")
const router = express.Router()


//1. Get All Articles
router.get('/', getAllArticles)
//2. Get Article by ID
router.get('/:articleId', getArticlebyId)
//3. Add Article
router.post('/', addArticle)
//4. Update Article
router.patch('/:articleId', updateArticle)
//5. Delete Article
router.delete('/:articleId', deleteArticle)

module.exports = router