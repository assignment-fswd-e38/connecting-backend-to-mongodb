const express = require("express")
const { getAllAuthors, getAuthorbyId, addAuthor, updateAuthor, deleteAuthor } = require("../Controllers/authorController")
const router = express.Router()

//1. Get All Authors
router.get('/', getAllAuthors)
//2. Get Author by ID
router.get('/:authorId', getAuthorbyId)
//3. Add Author
router.post('/', addAuthor)
//4. Update Author
router.patch('/:authorId', updateAuthor)
//5. Delete Author
router.delete('/:authorId', deleteAuthor)

module.exports = router