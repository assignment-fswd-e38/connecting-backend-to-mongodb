require('dotenv').config()
const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose');
const articleRoute = require("./Routes/articleRoute")
const authorRoute = require("./Routes/authorRoute")
const app = express()
const port = 3000

app.use(cors())
app.use(express.json());
app.use("/articles", articleRoute)
app.use("/authors", authorRoute)

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})


main().then(()=> console.log("Connected")).catch(err => console.log(err));
async function main() {
    await mongoose.connect(process.env.DB_URL);
  
    // use `await mongoose.connect('here paste our database connecting link');` 
    //if your database has auth enabled
  }