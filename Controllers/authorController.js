const Author = require("../Models/authorModel");

//1. Get All Authors
const getAllAuthors = async(req, res) => {
    const authors = await Author.find({});
    res.send(authors)
}
//2. Get Author by ID
const getAuthorbyId = async(req, res) => {
    const author = await Author.findById(req.params.authorId).exec();
    res.send(Author)
}
//3. Add Author
const addAuthor = async(req, res) => {
    const author = new Author(req.body)
    await author.save()
    res.send(author)
}
//4. Update Author
const updateAuthor = async(req, res) => {
    const updatedAuthor = await Author.findByIdAndUpdate(req.params.authorId, req.body, {new : true})
    res.send(updatedAuthor)
}
//5. Delete Author
const deleteAuthor = async(req, res) => {
    await Author.findOneAndDelete(req.params.authorId)
    res.send(`Deleted Author!`)
}

module.exports = {
    getAllAuthors,
    getAuthorbyId,
    addAuthor,
    updateAuthor,
    deleteAuthor
}