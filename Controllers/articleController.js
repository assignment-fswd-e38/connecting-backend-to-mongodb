const Article = require("../Models/articleModel");

//1. Get All Articles
const getAllArticles = async(req, res) => {
    const articles = await Article.find({});
    res.json(articles)
    }
//2. Get Article by ID
const getArticlebyId = async(req, res) => {
    const article = await Article.findById(req.params.articleId).exec();
    res.json(article)
}
//3. Add Article
const addArticle = async(req, res) => {
    //1. Using req.body create a document
      const article = new Article(req.body)
    //2. Save the document to database
        await article.save() 
     //here we have await so add async to addArticle
    //3. Adding article(created article) as res
      res.json(article)
}
//4. Update Article
const updateArticle = async(req, res) => {
    const updatedArticle = await Article.findByIdAndUpdate(req.params.articleId, req.body, {new: true});
    res.json(updatedArticle)
}
//5. Delete Article
const deleteArticle = async(req, res) => {
    await Article.findByIdAndDelete(req.params.articleId)
    res.send('Deleted Article!') //here we return a message only
}

module.exports = {
    getAllArticles,
    getArticlebyId,
    addArticle,
    updateArticle,
    deleteArticle
}